import React, { useState } from "react";
import NoteList from "./NoteList";


function App() {
  const [listElements, setListElements] = useState([]);
  const [listKey, setListKey] = useState(1);
  

  const addList = () => {
    const newListElement = {key: listKey, content: <NoteList key={listKey}></NoteList>, text: "List " + String(listKey) + "   "}
    setListElements([...listElements, newListElement]);
    setListKey(listKey + 1);
  };

  const deleteList = (key) => {
    const updatedListElements = listElements.filter((element) => element.key !== key);
    setListElements(updatedListElements);
  };

  return (
    <div className="nestedList">
      <button className="buttonListCreate" onClick={addList}>
        Create list
      </button>
      <ul>
        {listElements.map((curList) => (
          <div key={curList.key} className="listOfElements">
            {curList.text}
            {curList.content}
            <button className="buttonListRemove" onClick={() => deleteList(curList.key)}>Delete list</button>
          </div>
        ))}
      </ul>
    </div>
  );
}

export default App; 
