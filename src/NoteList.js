import React, { useState } from "react";


function NoteList() {
    const [noteElements, setNoteElements] = useState([]);
    const [noteKey, setNoteKey] = useState(1);
  
    const addNote = () => {
        const newNoteElement = { key: noteKey, content: 'Some todo here ' + String(noteKey) + '   '};
        setNoteElements([...noteElements, newNoteElement]);
        setNoteKey(noteKey + 1);
    };

    const deleteNote = (key) => {
        const updatedNoteElements = noteElements.filter((element) => element.key !== key);
        setNoteElements(updatedNoteElements);
    };
  
    return (
      <div>
        <button className="buttonNoteCreate" onClick={addNote}>Add Div</button>
        {noteElements.map((element) => (
            <div key={element.key}>
                {element.content}
                <button className="buttonNoteRemove" onClick={() => deleteNote(element.key)}>Delete note</button>
            </div>
        ))}
      </div>
    );
  }

export default NoteList;